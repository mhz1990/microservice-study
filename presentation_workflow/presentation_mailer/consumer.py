import json
import time
import pika

from pika.exceptions import AMQPConnectionError
import django
import os
import sys

# import time
from django.core.mail import send_mail


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "presentation_mailer.settings")
django.setup()


def process_approval(dictionary):
    send_mail(
        "Your presentation has been accepted",
        f"{dictionary['name']}, we're happy to tell you that your presentation {dictionary['title']} has been accepted",
        "admin@conference.go",
        [dictionary["email"]],
        fail_silently=False,
    )


def process_rejection(dictionary):
    send_mail(
        "Your presentation has been rejected",
        f"{dictionary['name']}, we regret to tell you that your presentation {dictionary['title']} has been rejected",
        "admin@conference.go",
        [dictionary["email"]],
        fail_silently=False,
    )


def process_message(ch, method, properties, body):
    dictionary = json.loads(body)
    if dictionary["status"] == "approved":
        process_approval(dictionary)
    if dictionary["status"] == "rejected":
        process_rejection(dictionary)


while True:
    try:
        parameters = pika.ConnectionParameters(host="rabbitmq")
        connection = pika.BlockingConnection(parameters)
        channel = connection.channel()
        channel.queue_declare(queue="tasks")
        channel.basic_consume(
            queue="tasks",
            on_message_callback=process_message,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ")
        time.sleep(2.0)
