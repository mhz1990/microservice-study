from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()


from attendees.models import AccountVO


def update_or_delete_account(ch, method, properties, body):
    try:
        content = json.loads(body.decode("utf-8"))
        first_name = content["first_name"]
        last_name = content["last_name"]
        email = content["email"]
        is_active = content["is_active"]
        updated_string = content["updated"]
        updated = datetime.fromisoformat(updated_string)
        if is_active:
            # Update or create the AccountVO object
            AccountVO.objects.update_or_create(
                email=email,
                defaults={
                    "first_name": first_name,
                    "last_name": last_name,
                    "is_active": is_active,
                    "updated": updated,
                },
            )
        else:
            # Delete the AccountVO object if it exists
            AccountVO.objects.filter(email=email).delete()
    except json.JSONDecodeError as e:
        print("Error decoding JSON message:", str(e))
    except KeyError as e:
        print("Error extracting values from JSON message:", str(e))
    except Exception as e:
        print("Error processing message:", str(e))


while True:
    try:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host="rabbitmq")
        )  # Replace "<rabbitmq_host>" with the actual hostname or IP address of your RabbitMQ server
        channel = connection.channel()
        channel.exchange_declare(
            exchange="account_info", exchange_type="fanout"
        )
        result = channel.queue_declare(queue="", exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange="account_info", queue=queue_name)
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_or_delete_account,
            auto_ack=True,
        )
        print("Waiting for messages. To exit, press CTRL+C")
        channel.start_consuming()
    except AMQPConnectionError:
        print("Could not connect to RabbitMQ. Retrying in a few seconds...")
        time.sleep(2)
